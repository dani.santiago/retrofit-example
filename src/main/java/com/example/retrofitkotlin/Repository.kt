package com.example.retrofitkotlin

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.retrofitkotlin.model.Character
import com.example.retrofitkotlin.model.Data
import com.example.retrofitkotlin.model.Planet
import com.example.retrofitkotlin.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    private val apiInterface = ApiInterface.create()
    var dataInfo = MutableLiveData<Data>()
    var planetInfo = MutableLiveData<Planet>()

    fun fetchData(url: String): MutableLiveData<Data> {
        val call = apiInterface.getData(url)
        call.enqueue(object: Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
                dataInfo.postValue(null)
            }

            override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
                if (response != null && response.isSuccessful) {
                    dataInfo.value = response.body()
                }
            }
        })
        return dataInfo
    }

    fun getPlanetData(url: String): MutableLiveData<Planet> {
        val call = apiInterface.getPlanet(url)
        call.enqueue(object: Callback<Planet> {
            override fun onFailure(call: Call<Planet>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
                planetInfo.postValue(null)
            }

            override fun onResponse(call: Call<Planet>, response: Response<Planet>) {
                if(response != null && response.isSuccessful){
                    planetInfo.value = response.body()
                }
            }
        })
        return planetInfo
    }
}
