package com.example.retrofitkotlin.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.retrofitkotlin.model.Character
import com.example.retrofitkotlin.Repository
import com.example.retrofitkotlin.model.Data
import com.example.retrofitkotlin.model.Planet

class MyViewModel: ViewModel() {
    val repository = Repository()
    var data = MutableLiveData<Data>()
    var actualCharacter = MutableLiveData<Character>()
    var planetData = MutableLiveData<Planet>()

    init {
        fetchData("people")
    }

    fun fetchData(url: String){
        data = repository.fetchData(url)
    }

    fun setCharacter(character: Character){
        planetData = repository.getPlanetData(character.homeworld)
        actualCharacter.value = character
    }
}