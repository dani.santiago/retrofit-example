package com.example.retrofitkotlin.model

data class Data(
    val count: Int,
    val next: String,
    val previous: String,
    val results: List<Character>
)