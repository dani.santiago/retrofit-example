package com.example.retrofitkotlin.model

import com.google.gson.annotations.SerializedName

data class Character(
    @SerializedName("birth_year")
    val birthYear: String,
    val gender: String,
    val height: String,
    val homeworld: String,
    val mass: String,
    val name: String,
    val url: String,
    @SerializedName("hair_color")
    val hairColor: String,
    @SerializedName("eye_color")
    val eyeColor: String,
    @SerializedName("skin_color")
    val skinColor: String
)