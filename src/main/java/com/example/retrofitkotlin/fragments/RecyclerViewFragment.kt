package com.example.retrofitkotlin.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.retrofitkotlin.model.Character
import com.example.retrofitkotlin.OnClickListener
import com.example.retrofitkotlin.viewmodel.MyViewModel
import com.example.retrofitkotlin.adapter.CharacterAdapter
import com.example.retrofitkotlin.databinding.FragmentRecyclerViewBinding
import com.example.retrofitkotlin.model.Data
import com.example.retrofitkotlin.R
import androidx.core.content.ContextCompat


class RecyclerViewFragment : Fragment(), OnClickListener {

    private lateinit var characterAdapter: CharacterAdapter
    private var layoutType = 0 //0 - LinearLayout, 1 - GridLayout
    lateinit var binding: FragmentRecyclerViewBinding
    private val viewModel: MyViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.data.value?.let {
            setUpRecyclerView(it, layoutType)
        }

        viewModel.data.observe(viewLifecycleOwner, Observer {
            if(viewModel.data.value == null){
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
                binding.progressBar.visibility = View.GONE
            }
            else{
                setUpRecyclerView(it, layoutType)
                if(viewModel.data.value!!.previous != null){
                    binding.previousButton.isEnabled = true
                }
                else{
                    binding.previousButton.isEnabled = false
                }
                if(viewModel.data.value!!.next != null){
                    binding.nextButton.isEnabled = true
                }
                else{
                    binding.nextButton.isEnabled = false
                }
            }
        })

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.data.value?.let {
                viewModel.fetchData(it.next)
            }
            binding.swipeRefresh.isRefreshing = false
        }

        binding.previousButton.isEnabled = false
        binding.nextButton.isEnabled = false
        binding.previousButton.setOnClickListener {
            viewModel.data.value?.let {
                viewModel.fetchData(it.previous)
            }
        }
        binding.nextButton.setOnClickListener {
            viewModel.data.value?.let {
                viewModel.fetchData(it.next)
            }
        }

        binding.changeLayoutFab.setOnClickListener {
            if(layoutType == 0){
                layoutType = 1
                binding.changeLayoutFab.setImageDrawable(ContextCompat.getDrawable(context!!, android.R.drawable.ic_dialog_dialer))
            }
            else{
                layoutType = 0
                binding.changeLayoutFab.setImageDrawable(ContextCompat.getDrawable(context!!, android.R.drawable.ic_menu_sort_by_size))
            }
            characterAdapter.setLayoutType(layoutType)
            setUpRecyclerView(viewModel.data.value!!, layoutType)
        }
    }

    private fun setUpRecyclerView(myData: Data, layoutType: Int) {
        characterAdapter = CharacterAdapter(myData.results, this)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = if(layoutType==0) LinearLayoutManager(context) else GridLayoutManager(context, 2)
            adapter = characterAdapter
        }
        binding.progressBar.visibility = View.GONE
    }

    override fun onClick(character: Character) {
        viewModel.setCharacter(character)
        findNavController().navigate(R.id.action_recyclerViewFragment_to_detailFragment)

    }
}