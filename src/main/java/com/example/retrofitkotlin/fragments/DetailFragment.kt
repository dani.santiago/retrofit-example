package com.example.retrofitkotlin.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.retrofitkotlin.databinding.FragmentDetailBinding
import com.example.retrofitkotlin.viewmodel.MyViewModel

class DetailFragment : Fragment() {

    lateinit var binding: FragmentDetailBinding
    private val viewModel: MyViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.actualCharacter.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding.tvName.text = it.name
            binding.tvGenderValue.text = it.gender
            binding.tvBirthyearValue.text = it.birthYear
            binding.tvHeightValue.text = it.height
            binding.tvMassValue.text = it.mass
            binding.tvHairColorValue.text = it.hairColor
            binding.tvEyeColorValue.text = it.eyeColor
            binding.tvSkinColorValue.text = it.skinColor
        })
        viewModel.planetData.observe(viewLifecycleOwner, Observer {
            binding.tvHomeworldValue.text = it.name
        })
    }
}