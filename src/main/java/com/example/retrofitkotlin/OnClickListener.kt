package com.example.retrofitkotlin

import com.example.retrofitkotlin.model.Character

interface OnClickListener {
    fun onClick(character: Character)
}